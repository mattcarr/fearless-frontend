import React from 'react';


class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {name: "", starts: "", ends: "", description: "", maxPresentations: "", maxAttendees: "", location: "", locations: []}
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleStartsChange = this.handleStartsChange.bind(this)
        this.handleEndsChange = this.handleEndsChange.bind(this)
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this)
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this)
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this)
        this.handleLocationChange = this.handleLocationChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleNameChange (event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleStartsChange (event) {
        const value = event.target.value;
        this.setState({starts: value})
    }

    handleEndsChange (event) {
        const value = event.target.value;
        this.setState({ends: value})
    }

    handleDescriptionChange (event) {
        const value = event.target.value;
        this.setState({description: value})
    }

    handleMaxPresentationsChange (event) {
        const value = event.target.value;
        this.setState({maxPresentations: value})
    }

    handleMaxAttendeesChange (event) {
        const value = event.target.value;
        this.setState({maxAttendees: value})
    }

    handleLocationChange (event) {
        const value = event.target.value;
        this.setState({location: value})
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = {...this.state}
        data.max_presentations = data.maxPresentations
        data.max_attendees = data.maxAttendees
        delete data.locations
        delete data.maxAttendees
        delete data.maxPresentations
        console.log(data)

        const conferenceURL = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceURL, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            const cleared = {name: "",
            starts: "",
            ends: "",
            description: "",
            maxPresentation: "",
            maxAttendees: "",
            location: "",
        }
        this.setState(cleared)
    }
}

    async componentDidMount() {
        const url = "http://localhost:8000/api/locations/";
        const response = await fetch(url);

        if(response.ok) {
            const data = await response.json()
            this.setState({locations: data.locations})
        }
    }
    render () {
        return (
            <div>
                <h1>Create a new conference</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" id="name" className="form-control" name="name" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleStartsChange} value={this.state.starts} placeholder="mm/dd/yyyy" required type="date" id="starts" className="form-control" name="starts" />
                    <label htmlFor="room_count">Starts</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleEndsChange} value={this.state.ends} placeholder="mm/dd/yyyy" required type="date" id="ends" className="form-control" name="ends" />
                    <label htmlFor="room_count">Ends</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="description">Description</label>
                    <textarea onChange={this.handleDescriptionChange} value={this.state.description} className="form-control" id="description" name="description" rows="5"></textarea>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleMaxPresentationsChange} value={this.state.maxPresentations} placeholder="Maximum presentations" required type="number" id="max_presentations" className="form-control" name="max_presentations" />
                    <label htmlFor="room_count">Max presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleMaxAttendeesChange} value={this.state.maxAttendees} placeholder="Maximum Attendees" required type="number" id="max_attendees" className="form-control" name="max_attendees" />
                    <label htmlFor="room_count">Max attendees</label>
                </div>
                <div className="mb-3">
                    <select onChange={this.handleLocationChange} value={this.state.location} required id="location" className="form-select" name="location">
                    <option value=""> Choose Location </option>
                    {this.state.locations.map(location => {
                        return (
                        <option key={location.id} value={location.id} id="location">
                        {location.name}
                        </option>
                        )
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
        )
    }
}

export default ConferenceForm
